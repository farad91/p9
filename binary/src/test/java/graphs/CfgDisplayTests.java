package graphs;

import java.io.IOException;

import p9.binary.cfg.display.CfgComponent;
import p9.binary.cfg.graph.AnalysisResult;
import tools.TestsHelper;
import binparse.Binary;

/**
 * Test various components of the CFG display.
 *
 * @author Bogdan Mihaila
 */
public class CfgDisplayTests {

  public static void main (String... args) throws IOException {
    crackaddr();
//    rreilExample();
  }

  public static void crackaddr () throws IOException {
    Binary binary = TestsHelper.get32bitExamplesBinary("crackaddr-bad-oversimplified");
    AnalysisResult result = TestsHelper.analyze(binary);
//    CfgComponent cfg = TestsHelper.buildRReilCfgComponent(result, "copy_it");
    CfgComponent cfg = TestsHelper.buildNativeCfgComponent(result, "copy_it");
    TestsHelper.showWindowWithGraph(cfg);
  }

  public static void rreilExample () throws IOException {
    Binary binary = TestsHelper.getRReilExamplesBinary("RReilExample.rreil");
    AnalysisResult result = TestsHelper.analyze(binary);
//  CfgView cfg = TestsHelper.buildRReilCfg(result, "copy_it");
    CfgComponent cfg = TestsHelper.buildRReilCfgComponent(result);
    TestsHelper.showWindowWithGraph(cfg);
  }

}

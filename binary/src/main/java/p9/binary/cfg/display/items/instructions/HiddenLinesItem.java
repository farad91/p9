package p9.binary.cfg.display.items.instructions;

import java.util.LinkedList;
import java.util.List;

import p9.binary.cfg.display.items.DrawableItem;
import p9.binary.cfg.display.items.TextItem;
import bindead.debug.StringHelpers;
import bindead.domainnetwork.channels.WarningMessage;
import com.googlecode.jatl.Html;
import java.io.StringWriter;

/**
 *
 * @author Raphael Dümig
 */
public class HiddenLinesItem extends TextItem {

  private final List<DrawableItem> lines;

  public HiddenLinesItem() {
    this(new LinkedList<DrawableItem>());
  }

  public HiddenLinesItem(List<DrawableItem> lines) {
    super("...");
    this.lines = lines;
  }

  public void addLine(DrawableItem di) {
    lines.add(di);
  }

  @Override
  public String toString() {
    return toString(false);
  }
  
  public String toString(boolean html) {
    if (!html) {
      LinkedList<InstructionItem> iitems = new LinkedList<>();
      for (DrawableItem ditem : lines) {
        if (ditem instanceof InstructionItem) {
          iitems.add((InstructionItem) ditem);
        }
      }
      return StringHelpers.joinWith("\n", InstructionItem.instructionItemsToStrings(iitems));
    }
    else {
      final StringWriter htmlStringWriter = new StringWriter();
      new Html(htmlStringWriter) {
        {
          indent(indentOff); // needed as Swing Tooltips HTML seem to choke on tab indented HTML strings
          html();
          body();
          // headline
          p().text("hidden instructions:").end(); // p
          table().attr("cellpadding", "0");
          for (DrawableItem item : lines) {
            tr();
            if (item instanceof InstructionItem) {
              InstructionItem iitem = (InstructionItem) item;
              for (String str : iitem.getInstructionStrings()) {
                td().style("padding: 0 5 0 0").text(str).end();
              }
            } else {
              td().style("padding: 0").attr("colspan", "4").text(item.toString()).end();
            }
            end(); // tr
          }
          end(); // table
          end(); // body
          end(); // html
          done();
        }
      };
      return htmlStringWriter.toString();
    }
  }
  
  @Override
  public String tooltipText() {
    return toString(true);
  }
}

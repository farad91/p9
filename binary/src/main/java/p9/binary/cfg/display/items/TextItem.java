package p9.binary.cfg.display.items;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.font.TextAttribute;
import java.awt.font.TextLayout;
import java.awt.geom.Point2D;
import java.text.AttributedString;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import p9.binary.cfg.display.ColorConfig;
import p9.binary.cfg.display.DrawHelper;
import p9.binary.cfg.display.items.highlight.DataHighlight;
import p9.binary.cfg.display.items.highlight.ObjectHighlight;
import p9.binary.cfg.display.items.highlight.VariableHighlight;

/**
 *
 * @author Raphael Dümig
 */
public class TextItem extends DrawableItem {
  public enum Align { RIGHT, CENTER, LEFT }
  private static final AttributedString defaultValue = new AttributedString("<empty>");

  // each line has its own TextLayout object
  private List<TextLayout> textLayout;
  private String text;

  private Font font;
  private Color textColor;
  private Align textAlign;

  protected boolean highlighted;
  private String search;


  public TextItem(String text) {
    this.text = text;
    this.font = null;
    this.textColor = null;
    this.search = null;
    this.textAlign = Align.LEFT;
  }

  @Override public void drawItem (Graphics2D g, Point2D position) {
    if (highlighted)
      DrawHelper.drawRectangleHighligh(g, position, getItemWidth(), getItemHeight());
    super.drawItem(g, position);
  }

  @Override
  public void draw(Graphics2D g, Point2D position) {
    if (textLayout == null) {
      updateTextLayout();
    }

    double verticalOffset = 0;
    
    for (TextLayout lineLayout : textLayout) {
      switch (textAlign) {
        case LEFT:
          lineLayout.draw(g, (float) position.getX(),
                  (float) (position.getY() + verticalOffset + lineLayout.getAscent()));
          break;
        case RIGHT:
          lineLayout.draw(g, (float) (position.getX() + getItemWidth() - lineLayout.getBounds().getWidth()),
                  (float) (position.getY() + verticalOffset + lineLayout.getAscent()));
          break;
        case CENTER:
          lineLayout.draw(g, (float) (position.getX() + (getItemWidth() - lineLayout.getBounds().getWidth()) / 2),
                  (float) (position.getY() + verticalOffset + lineLayout.getAscent()));
      }
      
      verticalOffset += lineLayout.getAscent() + lineLayout.getDescent();
    }
  }

  @Override
  public ItemSize getMinimumSize() {
    if (textLayout == null) {
      updateTextLayout();
    }
    ItemSize result = new ItemSize(0,0);
    
    for(TextLayout lineLayout : textLayout) {
      result.setSize(
              Math.max( result.getWidth(), lineLayout.getBounds().getWidth() ),
              result.getHeight() + lineLayout.getAscent() + lineLayout.getDescent()
      );
    }
    
    return result;
  }

  public void setText(String text) {
    this.text = text;
    updateTextLayout();
    updateSize();
  }

  @Override
  public void setFont(Font f) {
    this.font = f;
    updateTextLayout();
    update();
  }

  @Override
  public Font getFont () {
    return font;
  }

  @Override
  public void setTextColor(Color color) {
    this.textColor = color;
    updateTextLayout();
  }

  @Override
  public Color getTextColor() {
    return this.textColor;
  }

  public void setAlign(Align align) {
    this.textAlign = align;
  }

  public Align getAlign() {
    return this.textAlign;
  }

  protected List<AttributedString> getAttributedStrings() {
    
    LinkedList<AttributedString> result = new LinkedList<>();
    
    for (String line : text.split("\n")) {
      AttributedString attrText;

      if (!line.isEmpty()) {
        attrText = new AttributedString(line);
      
        if (textColor != null) {
          attrText.addAttribute(TextAttribute.FOREGROUND, textColor);
        }
        if (font != null) {
          // NOTE: setting the font directly disables some other text attributes, so we need to set it by setting its family
          // attrText.addAttribute(TextAttribute.FONT, font);
          attrText.addAttribute(TextAttribute.FAMILY, font.getFamily());
          attrText.addAttribute(TextAttribute.SIZE, font.getSize());
        }

        result.add(attrText);
      }
    }
    
    return result;
  }

  public void highlight (ObjectHighlight highlight) {
    // needs to be implemented in subclasses
  }

  public void disableHighlight (ObjectHighlight highlighting) {
    // needs to be implemented in subclasses
  }

  public void highlight (VariableHighlight highlightedVariable) {
    // needs to be implemented in subclasses
  }

  public void disableHighlight (VariableHighlight highlightedVariable) {
    // needs to be implemented in subclasses
  }

  @Override public void highlight (DataHighlight dataToHighlight) {
    if (dataToHighlight instanceof ObjectHighlight) {
      highlight((ObjectHighlight) dataToHighlight);
    } else if (dataToHighlight instanceof VariableHighlight) {
      highlight((VariableHighlight) dataToHighlight);
    }
    super.highlight(dataToHighlight);
  }

  @Override public void disableHighlight (DataHighlight highlighting) {
    if (highlighting instanceof ObjectHighlight) {
      disableHighlight((ObjectHighlight) highlighting);
    } else if (highlighting instanceof VariableHighlight) {
      disableHighlight((VariableHighlight) highlighting);
    }
    super.disableHighlight(highlighting);
  }

  private void highlightSearchQuery(List<AttributedString> attrText) {
    // highlight the search text
    if (search != null && !search.isEmpty()) {
      Iterator<AttributedString> attributedTextIter = attrText.iterator();
      
      for (String line : text.split("\n")) {
        String caseInsensitiveText = line.toLowerCase();
        int i = caseInsensitiveText.indexOf(search);
        AttributedString attrTextLine = attributedTextIter.next();
        
        while (i != -1) {
          attrTextLine.addAttribute(TextAttribute.BACKGROUND, ColorConfig.$TextHighlightBackground, i, i + search.length());
          i = caseInsensitiveText.indexOf(search, i + 1);
        }
      }
      
    }
  }

  private void updateTextLayout() {
    List<AttributedString> attrText = getAttributedStrings();
    // highlight the search query at last to overwrite formattings by child classes
    highlightSearchQuery(attrText);
    
    this.textLayout = new LinkedList<>();
    for (AttributedString attrLine : attrText) {
      this.textLayout.add(new TextLayout(attrLine.getIterator(), defaultContext.getFontRenderContext()));
    }
  }

  @Override
  public void setSearchQuery(String searchQuery, boolean selected) {
    if (searchQuery != null) {
      searchQuery = searchQuery.toLowerCase();
    }
    this.search = searchQuery;
    updateTextLayout();
  }

  // get the raw string
  public String getString() {
    return this.toString();
  }

  @Override
  public String toString() {
    return this.text;
  }
}

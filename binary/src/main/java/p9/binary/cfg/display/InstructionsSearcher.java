package p9.binary.cfg.display;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComponent;

import p9.binary.cfg.display.renderer.BasicBlockRenderer;
import p9.binary.cfg.graph.BasicBlock;
import p9.binary.cfg.graph.Cfg;
import prefuse.Display;
import prefuse.Visualization;
import prefuse.data.Graph;
import prefuse.data.Node;
import prefuse.data.Schema;
import prefuse.data.Table;
import prefuse.data.Tuple;
import prefuse.data.event.TupleSetListener;
import prefuse.data.query.SearchQueryBinding;
import prefuse.data.search.SearchTupleSet;
import prefuse.data.tuple.TupleSet;
import prefuse.util.FontLib;
import prefuse.util.ui.JSearchPanel;
import prefuse.visual.VisualItem;
import rreil.disassembler.Instruction;

public class InstructionsSearcher {
  public static String $SearchResult = "searchResult";
  private static String $Instructions = "instructions";
  private static String $InstructionText = "instructionText";
  private static String $InstructionNode = "instructionNode";
  private static String $InstructionIndex = "instructionLineInBlock";
  public static final Schema searcherSchema = new Schema(new String[] {$SearchResult},
    new Class[] {CfgBlockSearchHits.class});
  private final Visualization visualization;
  private final Display display;
  private final Graph graph;

  public InstructionsSearcher (Graph graph, Display display, InstructionTextFormatter formatter) {
    this.graph = graph;
    this.display = display;
    this.visualization = display.getVisualization();
    initSearchableInstructions(graph, formatter);
  }

  private static Table getTable (Graph graph, String key) {
    return (Table) graph.getSet(key);
  }

  private static void initSearchableInstructions (Graph graph, InstructionTextFormatter formatter) {
    Schema searchSchema = new Schema();
    searchSchema.addColumn($InstructionText, Object.class);
    searchSchema.addColumn($InstructionNode, Node.class);
    searchSchema.addColumn($InstructionIndex, int.class);
    graph.addSet($Instructions, new Table());
    Table instructionsTable = getTable(graph, $Instructions);
    instructionsTable.addColumns(searchSchema);

    for (Iterator<?> nodes = graph.nodes(); nodes.hasNext();) {
      Node node = (Node) nodes.next();
      int index = 0;
      BasicBlock instructions = (BasicBlock) node.get(Cfg.$Block);
      for (Instruction insn : instructions) {
        int row = instructionsTable.addRow();
        instructionsTable.set(row, $InstructionText, formatter.format(insn));
        instructionsTable.set(row, $InstructionNode, node);
        instructionsTable.setInt(row, $InstructionIndex, index);
        index++;
      }
    }
  }

  private static Map<Node, Set<Integer>> getResultLines (Tuple[] results) {
    Map<Node, Set<Integer>> resultLines = new HashMap<Node, Set<Integer>>();
    for (Tuple result : results) {
      Node node = (Node) result.get($InstructionNode);
      int instructionIndex = result.getInt($InstructionIndex);
      Set<Integer> lines = resultLines.get(node);
      if (lines == null) {
        lines = new HashSet<Integer>();
        resultLines.put(node, lines);
      }
      lines.add(instructionIndex);
    }
    return resultLines;
  }

  private void annotateNodesWithResults (QuerySearchResults<Tuple> searchResults) {
    Map<Node, Set<Integer>> resultLines = getResultLines(searchResults.getAll());
    for (Entry<Node, Set<Integer>> entry : resultLines.entrySet()) {
      Node node = entry.getKey();
      CfgBlockSearchHits hits = new CfgBlockSearchHits(entry.getValue(), searchResults.getQuery());
      node.set($SearchResult, hits);
      setNodeHighlight(node, true);
    }
  }

  private void highlightSearchResults (Tuple chosenResult, QuerySearchResults<Tuple> searchResults, int animationDuration, boolean showLocation) {
    annotateNodesWithResults(searchResults);
    int instructionIndex = chosenResult.getInt($InstructionIndex);
    Node selectedNode = (Node) chosenResult.get($InstructionNode);
    VisualItem visualNode = visualization.getVisualItem(CfgView.$Nodes, selectedNode);
    BasicBlock instructions = (BasicBlock) selectedNode.get(Cfg.$Block);
    Instruction selectedInstr = instructions.get(instructionIndex);
    
    if (showLocation) {
      ((CfgBlockSearchHits) selectedNode.get($SearchResult)).setSelectedResult(selectedInstr);
      ((BasicBlockRenderer) visualNode.getRenderer()).panToLine(instructionIndex, visualNode, display, animationDuration);
    }
    display.damageReport();
    visualization.run(CfgView.$Repaint);
  }

  private void setNodeHighlight (Node node, boolean highlight) {
    VisualItem visualNode = visualization.getVisualItem(CfgView.$Nodes, node);
    visualNode.setFixed(highlight); // use this property as the highlight is set by the neighbor control
  }

  public JComponent buildSearchPanel () {
    final Visualization visualization = display.getVisualization();
    final QuerySearchResults<Tuple> searchResults = new QuerySearchResults<Tuple>();
    final AbstractAction showPrevResultAction = new AbstractAction("prev") {
      @Override public void actionPerformed (ActionEvent e) {
        highlightSearchResults(searchResults.getPrevious(), searchResults, 200, true);
      }
    };
    final AbstractAction showNextResultAction = new AbstractAction("next") {
      @Override public void actionPerformed (ActionEvent e) {
        highlightSearchResults(searchResults.pop(), searchResults, 200, true);
      }
    };
    final JButton prevResultButton = buildResultsButton(showPrevResultAction);
    final JButton nextResultButton = buildResultsButton(showNextResultAction);

    Table searchTable = getTable(graph, $Instructions);
    SearchQueryBinding searchQuery = new SearchQueryBinding(searchTable, $InstructionText);
    final SearchTupleSet searchSet = searchQuery.getSearchSet();
    visualization.addFocusGroup(Visualization.SEARCH_ITEMS, searchSet);

    searchSet.addTupleSetListener(new TupleSetListener() {
      @Override public void tupleSetChanged (TupleSet t, Tuple[] add, Tuple[] rem) {
        // see if there are changes to the old search results
        String query = ((SearchTupleSet) t).getQuery();
        if (Arrays.deepEquals(add, rem) && query.equals(searchResults.getQuery()))
          return;
        for (Tuple tuple : rem) {
          Node node = (Node) tuple.get($InstructionNode);
          node.set($SearchResult, null);
          setNodeHighlight(node, false);
        }
        if (add.length > 0) {
          searchResults.init(add, query);
          showNextResultAction.setEnabled(true);
          prevResultButton.setEnabled(true);
          nextResultButton.setEnabled(true);
          highlightSearchResults(searchResults.peek(), searchResults, 100, false);
        } else {
          showNextResultAction.setEnabled(false);
          prevResultButton.setEnabled(false);
          nextResultButton.setEnabled(false);
          visualization.run(CfgView.$Repaint);
        }
      }
    });
    JSearchPanel searchInputField = new JSearchPanel(searchSet, $InstructionText, true) {
      @Override public void actionPerformed (ActionEvent e) {
        super.actionPerformed(e);
        if (showNextResultAction.isEnabled())
          showNextResultAction.actionPerformed(e);
      }
    };
    searchInputField.setShowResultCount(true);
    searchInputField.setBorder(BorderFactory.createEmptyBorder(5, 5, 4, 5));
    searchInputField.setFont(FontLib.getFont("Tahoma", Font.PLAIN, 11));
    searchInputField.setBackground(ColorConfig.$Background);
    searchInputField.setForeground(ColorConfig.$Foreground);
    searchInputField.setQuery(null);
    Box box = new Box(BoxLayout.X_AXIS);
    box.add(prevResultButton);
    box.add(Box.createHorizontalStrut(5));
    box.add(nextResultButton);
    box.add(searchInputField);
    box.setBackground(ColorConfig.$Background);
    box.setForeground(ColorConfig.$Foreground);
    box.setOpaque(true);
    return box;
  }

  private static JButton buildResultsButton (final AbstractAction action) {
    JButton button = new JButton(action);
    button.setFont(FontLib.getFont("Tahoma", Font.BOLD, 12));
    button.setEnabled(false);
    button.setOpaque(false);
    button.setContentAreaFilled(false);
    button.setFocusPainted(false);
    button.setBorderPainted(false);
    button.setBorder(BorderFactory.createEmptyBorder(7, 4, 3, 0));
    button.setMaximumSize(new Dimension(100, 20));
    return button;
  }

  public static class CfgBlockSearchHits {
    public Instruction selectedInstruction;
    public final String searchQuery;
    public final Set<Integer> lineIndicesOfHits;

    public CfgBlockSearchHits (Set<Integer> lineIndicesOfHits, String searchQuery) {
      this.selectedInstruction = null;
      this.lineIndicesOfHits = lineIndicesOfHits;
      this.searchQuery = searchQuery;
    }

    public void setSelectedResult(Instruction selectedInstr) {
      selectedInstruction = selectedInstr;
    }

  }

  private static class QuerySearchResults<T> {
    private T[] results;
    private int index;
    private String searchQuery = "";

    public void init (T[] results, String query) {
      this.results = results;
      this.index = 0;
      this.searchQuery = query;
    }

    public T getPrevious () {
      index = (index + results.length - 1) % results.length;
      return results[index];
    }

    public T peek () {
      return results[index];
    }

    public T pop () {
      int currentIndex = index;
      index = (index + 1) % results.length;
      return results[currentIndex];
    }

    public T[] getAll () {
      return results;
    }

    public String getQuery () {
      return searchQuery;
    }
  }

  /**
   * Use to implement a wrapper for the indexing of instructions. The wrapper can either delegate to an implementation
   * that will produce the string only when queried.
   */
  public interface InstructionTextFormatter {
    public Object format (Instruction insn);
  }
}

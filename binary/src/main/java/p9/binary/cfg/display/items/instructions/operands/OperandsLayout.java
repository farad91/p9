
package p9.binary.cfg.display.items.instructions.operands;

import p9.binary.cfg.display.items.DrawableItem;
import p9.binary.cfg.display.items.ListLayout;
import p9.binary.cfg.display.items.TextItem;

/**
 *
 * @author Raphael Dümig <duemig@in.tum.de>
 */
public class OperandsLayout extends ListLayout {

  private static final TextItem separator = new TextItem(",");
  static {
    separator.setPadding(DrawableItem.PADDING_LEFT, 2);
    separator.setPadding(DrawableItem.PADDING_RIGHT, 10);
  }
  
  public OperandsLayout() {
    super(separator);
  }
}

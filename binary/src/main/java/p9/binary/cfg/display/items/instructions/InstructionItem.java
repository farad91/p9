package p9.binary.cfg.display.items.instructions;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Stroke;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;

import p9.binary.cfg.display.ColorConfig;
import p9.binary.cfg.display.RunAfter;
import p9.binary.cfg.display.StrokeConfig;
import p9.binary.cfg.display.items.CommentItem;
import p9.binary.cfg.display.items.ContainerItem;
import p9.binary.cfg.display.items.DrawableItem;
import p9.binary.cfg.display.items.HorizontalLayout;
import p9.binary.cfg.display.items.highlight.DataHighlight;
import p9.binary.cfg.display.items.highlight.InstructionHighlight;
import p9.binary.cfg.display.items.highlight.WarningsHighlight;
import p9.binary.cfg.display.items.instructions.operands.OperandsItem;
import p9.binary.cfg.display.items.instructions.operands.rreil.RReilOperandsItem;
import p9.binary.cfg.display.items.instructions.operands.x86.NativeOperandsItem;
import rreil.disassembler.Instruction;
import rreil.lang.RReilAddr;
import bindead.domainnetwork.channels.WarningsContainer;

/**
 *
 * @author Raphael Dümig
 */
public class InstructionItem extends ContainerItem {

  private final Instruction instruction;
  private final boolean nativeInstruction;

  private final InstructionDecoratorItem decorators;
  private final AddressItem address;
  private final MnemonicItem mnemonic;
  private final OpcodeItem opcode;
  private final OperandsItem operands;
  private CommentItem comment;

  private boolean hovered;
  private WarningsHighlight warningsHighlight;
  private final HighlightInstructionAnimation highlightAnimation;

  public InstructionItem(Instruction instruction, boolean nativeInstruction, boolean numbersInHex) {
    super(new HorizontalLayout());

    this.instruction = instruction;
    this.nativeInstruction = nativeInstruction;

    this.highlightAnimation = new HighlightInstructionAnimation(3000);
    this.highlightAnimation.setFinished();
    this.hovered = false;
    this.warningsHighlight = null;

    decorators = new InstructionDecoratorItem();
    address = new AddressItem(instruction.address());
    opcode = nativeInstruction ? new OpcodeItem(instruction)
                               : null;
    mnemonic = new MnemonicItem(instruction.mnemonic());

    boolean leaInstr = instruction.mnemonic().toLowerCase().equals("lea");
    operands = nativeInstruction ? new NativeOperandsItem(instruction, numbersInHex, leaInstr)
                                 : new RReilOperandsItem(instruction, numbersInHex);

    address.setPadding(PADDING_RIGHT, 5);
    if (opcode != null) {
      opcode.setPadding(PADDING_HORIZONTAL, 5);
    }
    mnemonic.setPadding(PADDING_HORIZONTAL, 5);
    operands.setPadding(PADDING_LEFT, 5);
    updateItems();

    setPadding(PADDING_ALL, 2);
  }

  public Instruction getInstruction() {
    return instruction;
  }

  public AddressItem getAddressItem() {
    return address;
  }

  public OpcodeItem getOpcodeItem() {
    return opcode;
  }

  public MnemonicItem getMnemonicItem() {
    return mnemonic;
  }

  public OperandsItem getOperandsItem() {
    return operands;
  }

  public InstructionDecoratorItem getDecoratorItem() {
    return decorators;
  }

  public void setCommentItem(CommentItem citem) {
    comment = citem;
    updateItems();
  }

  public CommentItem getCommentItem() {
    return comment;
  }

  public void removeComment() {
    comment = null;
    updateItems();
  }

  private void updateItems() {
    removeAllItems();

    LinkedList<DrawableItem> tempList = new LinkedList<>();
    if (decorators != null) {
      tempList.add(decorators);
    }
    if (address != null) {
      tempList.add(address);
    }
    if (opcode != null) {
      tempList.add(opcode);
    }
    if (mnemonic != null) {
      tempList.add(mnemonic);
    }
    if (operands != null) {
      tempList.add(operands);
    }
    if (comment != null) {
      tempList.add(comment);
    }
    addAllItems(tempList);
  }

  private void updateBackgroundColor() {
    if (highlightAnimation.hasFinished()) {
      if (hovered) {
        setBackgroundColor(ColorConfig.$MouseHoverHighlight);
      } else if (warningsHighlight != null) {
        setBackgroundColor(ColorConfig.$InsnWarning);
      } else {
        disableBackgroundColor();
      }
    }
  }
  
  public boolean startsBlock() {
    return instruction.address().offset() == 0;
  }

  public String[] getInstructionStrings() {
    String[] result = new String[5];

    result[0] = address.getAddressString();
    result[1] = nativeInstruction ? opcode.getOpcodeString() : "";
    result[2] = mnemonic.getMnemonic();
    result[3] = operands.getOperandsString();
    result[4] = (comment != null) ? comment.toString() : "";

    return result;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append(address.getAddressString());
    builder.append("  ");
    if (opcode != null) {
      builder.append(opcode.getOpcodeString());
      builder.append("  ");
    }
    builder.append(mnemonic.getMnemonic());
    builder.append("  ");
    builder.append(operands.getOperandsString());
    if (comment != null) {
      builder.append("  ");
      builder.append(comment.toString());
    }

    return builder.toString();
  }

  // mouseEntered, mouseExited: create the highlighting of the hovered instruction
  @Override
  public void mouseEntered(MouseEvent me, Point2D relPos) {
    hovered = true;
    updateBackgroundColor();
    super.mouseEntered(me, relPos);
  }

  @Override
  public void mouseExited(MouseEvent me, Point2D relPos) {
    hovered = false;
    updateBackgroundColor();
    super.mouseExited(me, relPos);
  }

  public void highlight(WarningsHighlight hwarnings) {
    RReilAddr ownAddr = getInstruction().address();

    if (hwarnings.contains(ownAddr)) {
      warningsHighlight = hwarnings;
      WarningsContainer warningsCollection = warningsHighlight.getWarningsCollection(ownAddr);
      int iteration = warningsHighlight.getIterationCount(ownAddr);
      decorators.setWarnings(warningsCollection, iteration);
      decorators.update();
      updateBackgroundColor();
    }
  }

  public void highlight(InstructionHighlight ih) {
    if (ih.contains(instruction)) {
      // use highlight animation
      highlightAnimation.start();
    }
  }

  @Override
  public void highlight(DataHighlight highlight) {
    if (highlight instanceof WarningsHighlight) {
      highlight((WarningsHighlight) highlight);
    } else if (highlight instanceof InstructionHighlight) {
      highlight((InstructionHighlight) highlight);
    } else {
      super.highlight(highlight);
    }
  }

  public void disableHighlight(WarningsHighlight hwarnings) {
    if (hwarnings.equals(warningsHighlight)) {
      warningsHighlight = null;
      updateBackgroundColor();
      decorators.setWarnings(null, 0);
      decorators.update();
    }
  }

  @Override
  public void disableHighlight(DataHighlight dataToHighlight) {
    if (dataToHighlight instanceof WarningsHighlight) {
      disableHighlight((WarningsHighlight) dataToHighlight);
    }
    super.disableHighlight(dataToHighlight);
  }

  @Override
  public JPopupMenu contextMenu(MouseEvent me, Point2D relPos) {
    JPopupMenu menu = super.contextMenu(me, relPos);

    JMenuItem insertCommentAfterInstr = new JMenuItem("Append comment to instruction");
    insertCommentAfterInstr.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        NotifyDescriptor.InputLine nd = new NotifyDescriptor.InputLine("Comment", "New comment");
        // display the dialog
        if (DialogDisplayer.getDefault().notify(nd) == NotifyDescriptor.OK_OPTION) {
          // get the text from the user and create an inline comment
          CommentItem comment = new CommentItem(nd.getInputText(), true);
          // update the text color of the item
          comment.setTextColor(null);
          comment.setFont(InstructionItem.this.getFont());
          setCommentItem(comment);
          InstructionItem.this.getViewer().redraw();
        }
      }
    });
    menu.add(insertCommentAfterInstr);

    if (comment != null) {
      JMenuItem removeCommentAfterInstr = new JMenuItem("Delete instruction comment");
      removeCommentAfterInstr.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent ae) {
          removeComment();
          InstructionItem.this.getViewer().redraw();
        }
      });
      menu.add(removeCommentAfterInstr);
    }

    return menu;
  }

  public boolean isHovered() {
    return hovered;
  }

  public boolean isHighlighted() {
    return !highlightAnimation.hasFinished();
  }

  // we need to override drawItem instead of draw, since we want to draw in
  // the whole item, including its padding
  @Override
  public void drawItem(Graphics2D g, Point2D position) {
    super.drawItem(g, position);

    Stroke oldStroke = g.getStroke();
    Color oldColor = g.getColor();
    g.setStroke(StrokeConfig.$ColumnSeparator);
    g.setColor(ColorConfig.$ColumnSeparator);

    // draw the separators before the opcode (if present) and the mnemonics
    if (opcode != null) {
      Point2D ocPos = getPosition(opcode);
      double x = position.getX() + ocPos.getX();
      double y = position.getY();
      g.draw(new Line2D.Double(x, y, x, y + getItemSize().getHeight()));
    }

    Point2D mnPos = getPosition(mnemonic);
    double x = position.getX() + mnPos.getX();
    double y = position.getY();
    g.draw(new Line2D.Double(x, y, x, y + getItemSize().getHeight()));

    g.setStroke(oldStroke);
    g.setColor(oldColor);
  }

  public static List<String> instructionItemsToStrings(Iterable<InstructionItem> items) {
    LinkedList<String> result = new LinkedList<>();

    LinkedList<String[]> instructionStrings = new LinkedList<>();
    int[] sizes = new int[4];

    for (InstructionItem iitem : items) {
      String[] strings = iitem.getInstructionStrings();

      for (int i = 0; i < 4; i++) {
        if (strings[i].length() > sizes[i]) {
          sizes[i] = strings[i].length();
        }
      }
      instructionStrings.add(strings);
    }

    for (String[] strings : instructionStrings) {
      StringBuilder builder = new StringBuilder();
      for (int i = 0; i < 4; i++) {
        if (strings[i].isEmpty()) {
          continue;
        }

        builder.append(strings[i]);

        // whitespace padding
        int spaces = 2 + sizes[i] - strings[i].length();
        for (int j = 0; j < spaces; j++) {
          builder.append(' ');
        }
      }
      result.add(builder.toString());
    }

    return result;
  }


  private class HighlightInstructionAnimation {

    private final int animationDuration;
    private final InstructionItem iitem;
    private boolean finished;

    public HighlightInstructionAnimation(int animationDuration) {
      this.animationDuration = animationDuration;
      this.iitem = InstructionItem.this;
      this.finished = false;
    }

    public void start() {
      this.finished = false;
      addHighlight();
      RunAfter.delay(animationDuration, new Runnable() {
        @Override
        public void run() {
          removeHighlight();
          finished = true;
        }
      });
    }

    private void addHighlight() {
      iitem.setBackgroundColor(ColorConfig.$RectangleHighlight);
      iitem.getViewer().redraw();
    }

    private void removeHighlight() {
      if(iitem.isHovered()) {
        iitem.setBackgroundColor(ColorConfig.$MouseHoverHighlight);
      } else if (iitem.warningsHighlight != null) {
        iitem.setBackgroundColor(ColorConfig.$InsnWarning);
      } else {
        iitem.disableBackgroundColor();
      }
      iitem.getViewer().redraw();
    }

    public boolean hasFinished() {
      return finished;
    }

    public void setFinished() {
      finished = true;
    }
  }

}

package p9.binary.cfg.display.items.instructions.operands.rreil;

import java.awt.event.MouseEvent;
import java.awt.font.TextAttribute;
import java.awt.geom.Point2D;
import java.text.AttributedString;
import java.util.List;

import p9.binary.cfg.display.ColorConfig;
import p9.binary.cfg.display.items.AttributedTextItem;
import p9.binary.cfg.display.items.highlight.VariableHighlight;
import rreil.lang.Rhs;
import rreil.lang.Rhs.Rlit;

/**
 * @author Raphael Dümig
 */
public class RReilOperandItem extends AttributedTextItem {
  private final Rhs operand;

  public RReilOperandItem (Rhs operand, boolean numbersInHex) {
    super(operandToString(operand, numbersInHex));
    this.operand = operand;
  }

  private static String operandToString (Rhs operand, boolean numbersInHex) {
    if (numbersInHex && operand instanceof Rlit) {
      return ((Rlit) operand).toHexString();
    } else {
      return operand.toString();
    }
  }

  public Rhs getOperand () {
    return operand;
  }
  
  @Override public void mouseEntered (MouseEvent me, Point2D relPos) {
    super.mouseEntered(me, relPos);
    getViewer().highlight(new VariableHighlight(operand));
  }

  @Override public void mouseExited (MouseEvent me, Point2D relPos) {
    super.mouseExited(me, relPos);
    getViewer().disableHighlight(new VariableHighlight(operand));
  }

  @Override public void highlight (VariableHighlight variableHighlight) {
    if (variableHighlight.contains(operand)) {
      highlighted = true;
    }
  }

  @Override public void disableHighlight (VariableHighlight variableHighlight) {
    if (variableHighlight.contains(operand)) {
      highlighted = false;
    }
  }

  @Override public List<AttributedString> getAttributedStrings () {
    List<AttributedString> result = super.getAttributedStrings();

    int i = getString().lastIndexOf(":");
    int l = getString().length();
    // style the register or constant name
    // TODO: use different colors for registers and constants/intervals
    addAttribute(result, getString(), TextAttribute.FOREGROUND, ColorConfig.$Register, 0, i);
    addAttribute(result, getString(), TextAttribute.FOREGROUND, ColorConfig.$RReilSizeAnnotation, i, l);
    addAttribute(result, getString(), TextAttribute.SIZE, getFont().getSize() - 4, i, l);
    return result;
  }
  
  
  public static RReilOperandItem toItem(Rhs operand, boolean numbersInHex) {
    if (operand instanceof Rhs.Rvar) {
      return new RReilVariableItem((Rhs.Rvar) operand, numbersInHex);
    } else {
      return new RReilOperandItem(operand, numbersInHex);
    }
  }

}

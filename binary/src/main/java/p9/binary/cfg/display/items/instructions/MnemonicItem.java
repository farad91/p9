package p9.binary.cfg.display.items.instructions;

import java.awt.Color;
import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;

import p9.binary.cfg.display.ColorConfig;
import p9.binary.cfg.display.items.TextItem;
import p9.binary.cfg.display.items.highlight.ObjectHighlight;

/**
 * @author Raphael Dümig
 */
public class MnemonicItem extends TextItem {
  private final String mnemonic;

  public MnemonicItem (String mnemonic) {
    super(mnemonic);
    this.mnemonic = mnemonic;
    super.setTextColor(ColorConfig.$Mnemonic);
  }

  public String getMnemonic () {
    return mnemonic;
  }

  @Override public void setTextColor (Color color) {
    // text color is set in constructor and should not be changed anymore
  }

  @Override public void mouseEntered (MouseEvent me, Point2D relPos) {
    super.mouseEntered(me, relPos);
    getViewer().highlight(new ObjectHighlight(mnemonic));
  }

  @Override public void mouseExited (MouseEvent me, Point2D relPos) {
    super.mouseExited(me, relPos);
    getViewer().disableHighlight(new ObjectHighlight(mnemonic));
  }

  @Override public void highlight (ObjectHighlight highlight) {
    if (highlight.contains(mnemonic)) {
      highlighted = true;
    }
  }

  @Override public void disableHighlight (ObjectHighlight highlighting) {
    if (highlighting.contains(mnemonic)) {
      highlighted = false;
    }
  }

}

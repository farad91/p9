
package p9.binary.cfg.display.items.instructions;

import java.awt.Color;
import java.awt.font.TextAttribute;
import java.text.AttributedString;
import java.util.List;

import p9.binary.cfg.display.ColorConfig;
import p9.binary.cfg.display.items.AttributedTextItem;
import rreil.disassembler.Instruction;

/**
 *
 * @author Raphael Dümig <duemig@in.tum.de>
 */
public class OpcodeItem extends AttributedTextItem {

    private final Instruction instr;

    public OpcodeItem(Instruction insn) {
        super( opcodeToString(insn) );

        this.instr = insn;
    }

    public byte[] getOpcode() {
        return instr.opcode();
    }

    public String getOpcodeString() {
        return opcodeToString(instr);
    }

    @Override
    public void setTextColor(Color color) {
        super.setTextColor(ColorConfig.$Opcode);
    }

    private static String opcodeToString(Instruction insn) {
        StringBuilder opcodeText = new StringBuilder(16);
        insn.opcode(opcodeText);
        return opcodeText.toString();
    }

    @Override
    public List<AttributedString> getAttributedStrings() {
      List<AttributedString> result = super.getAttributedStrings();
      addAttribute(result, getString(), TextAttribute.POSTURE, TextAttribute.POSTURE_OBLIQUE, 0, getString().length());
      return result;
    }

}

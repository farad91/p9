package p9.binary.cfg.graph;

import java.util.HashMap;
import java.util.Iterator;
import java.util.NavigableSet;
import java.util.TreeSet;

import javalx.data.Option;
import javalx.data.products.P2;
import prefuse.data.Graph;
import prefuse.data.Node;
import prefuse.data.Schema;
import rreil.disassembler.Instruction;
import rreil.lang.RReilAddr;
import bindead.analyses.BinaryCodeCache;
import bindead.analyses.algorithms.data.CallString;
import bindead.analyses.algorithms.data.CallString.Transition;
import binparse.Binary;
import binparse.Symbol;

/**
 * A control flow graph.
 *
 * @author Bogdan Mihaila
 */
public abstract class Cfg extends Graph {
  public static final String $Block = "block";
  public static final Schema $DataSchema = new Schema();
  private final AnalysisResult analysis;
  private final CallString callString;
  private HashMap<RReilAddr, P2<Node, Integer>> addressesToBlocks;

  static {
    $DataSchema.addColumn($Block, BasicBlock.class);
  }

  public Cfg (AnalysisResult analysis, CallString callString) {
    super(true);
    this.analysis = analysis;
    this.callString = callString;
    getNodeTable().addColumns($DataSchema);
  }

  public abstract boolean isRReilCfg ();

  /**
   * Returns the basic block that is stored at the given node.
   */
  public static BasicBlock getBasicBlock (Node node) {
    return (BasicBlock) node.get($Block);
  }

  public static String getName (AnalysisResult analysis, CallString callString) {
    if (callString.isRoot())
      return "[]";
    Transition transition = callString.peek();
    RReilAddr callToCfg = transition.getTarget(); // a procedure is identified by the call entry that it has, thus target here
    BinaryCodeCache binaryCodeView = analysis.getAnalysis().getBinaryCode();
    if (binaryCodeView == null)
      return callToCfg.toString();
    Binary binary = binaryCodeView.getBinary();
    Option<Symbol> symbol = binary.getSymbol(callToCfg.base());
    if (symbol.isSome())
      return symbol.get().toString();
    return callToCfg.toString();
  }

  public String getName () {
    return getName(analysis, callString);
  }

  public AnalysisResult getAnalysis () {
    return analysis;
  }

  public CallString getCallString () {
    return callString;
  }

  /**
   * Return an iterator over all the basic blocks in this CFG.
   */
  public Iterable<BasicBlock> getBasicBlocks () {
    Iterable<BasicBlock> iterable = new Iterable<BasicBlock>() {

      @Override public Iterator<BasicBlock> iterator () {
        Iterator<BasicBlock> iterator = new Iterator<BasicBlock>() {
          Iterator<?> nodes = Cfg.this.nodes();

          @Override public boolean hasNext () {
            return nodes.hasNext();
          }

          @Override public BasicBlock next () {
            Node node = (Node) nodes.next();
            return Cfg.getBasicBlock(node);
          }

          @Override public void remove () {
            nodes.remove();
          }
        };
        return iterator;
      }
    };
    return iterable;
  }

  /**
   * Return all the addresses of the instructions in this CFG.
   */
  public NavigableSet<RReilAddr> getAddresses () {
    NavigableSet<RReilAddr> addresses = new TreeSet<RReilAddr>();
    for (BasicBlock basicBlock : getBasicBlocks()) {
      for (Instruction insn : basicBlock) {
        addresses.add(insn.address());
      }
    }
    return addresses;
  }

  private void initBasicBlocksMap() {
    addressesToBlocks = new HashMap<>();
    for (Iterator<?> nodes = nodes(); nodes.hasNext();) {
      Node node = (Node) nodes.next();
      int index = 0;
      BasicBlock instructions = (BasicBlock) node.get(Cfg.$Block);
      for (Instruction insn : instructions) {
        RReilAddr address = insn.address();
        addressesToBlocks.put(address, new P2<>(node, index));
        index++;
      }
    }
  }

  public P2<BasicBlock, Integer> getBasicBlockContaining (RReilAddr address) {
    P2<Node, Integer> location = getNodeContaining(address);
    if (location == null)
      return null;
    return P2.tuple2(getBasicBlock(location._1()), location._2());
  }

  public P2<Node, Integer> getNodeContaining (RReilAddr address) {
    if (addressesToBlocks == null)
      initBasicBlocksMap();
    return addressesToBlocks.get(address);
  }

}

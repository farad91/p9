package p9.binary;

import binparse.Binary;
import binparse.BinaryFactory;
import java.beans.PropertyVetoException;
import java.io.IOException;
import javax.swing.Action;
import org.openide.filesystems.FileChangeAdapter;
import org.openide.filesystems.FileEvent;
import org.openide.loaders.DataNode;
import org.openide.loaders.DataObject;
import org.openide.nodes.Children;
import org.openide.util.Exceptions;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;
import org.openide.util.lookup.ProxyLookup;

/**
 * Visual presentation layer for binary objects as nodes.
 */
public class BinaryObjectNode extends DataNode {
  private Binary binary;
  private String label;

  public BinaryObjectNode (final DataObject obj, InstanceContent mutableLookup, final BinaryFactory binaryFactory) {
    super(obj, Children.LEAF, new ProxyLookup(obj.getLookup(), new AbstractLookup(mutableLookup)));
    buildBinaryObjectRootNode(obj.getPrimaryFile().getPath(), binaryFactory);
    mutableLookup.add(binary);
    obj.getPrimaryFile().addFileChangeListener(new FileChangeAdapter() {
      @Override public void fileChanged (FileEvent fe) {
        setChildren(Children.LEAF);
        buildBinaryObjectRootNode(fe.getFile().getPath(), binaryFactory);
        try {
          obj.setValid(false);
          obj.setValid(true);
        } catch (PropertyVetoException e) {
          Exceptions.printStackTrace(e);
        }
      }
    });
  }

  private void buildBinaryObjectRootNode (String path, BinaryFactory binaryFactory) {
    try {
      binary = binaryFactory.getBinary(path);
      label = "<font color='!textText'>" + super.getDisplayName() + "</font>     " +
        "<font color='!controlDkShadow'><i>" + binary.getFileFormat() + " " + binary.getType() + "</i> "
          + binary.getArchitectureName() + "</font>";
    } catch (IOException ex) {
      Exceptions.printStackTrace(ex);
    }
  }

  @Override public String getHtmlDisplayName () {
    return label;
  }

  @Override public Action getPreferredAction () {
    return new OpenBinaryAction(binary);
  }

}
